/* Version 1.6.1 To view an uncompressed version of this file, please visit: http://ioc.uncg.edu/incuncg/ */
$(document).ready(function(){
	UNCG.SearchSetup();

	$('ul#navlist-yellow').superfish();

	UNCG.TranslateSetup();
});

var UNCG = {
		SearchSetup: function(){
			$('#q').focus(function(){
				 if(this.value=='Search UNCG')this.value='';
			});
		
			if(!$('meta[name=unit]')[0])return;
			
			var unit = $('meta[name=unit]').attr('content');
			$('input[name=site]').remove();
			var radios = '<div id="searchInContainer"><label><input id="eduSearch" name="site" type="radio"x value="default_collection" checked="checked" /> UNCG.edu</label><label><input id="siteSearch" name="site" type="radio" value="site"/> this site</label></div>';
			$('#searchSub').after(radios);
			$('#siteSearch').val(unit);
			$('[name=site]:radio').change(
				function(){
					
					if($(this).attr('id')=='eduSearch'){
						$('input[name=client]').val('default_frontend');
						$('input[name=proxystylesheet]').val('default_frontend');
					}else{
						var unit = $('meta[name=unit]').attr('content');
						$('input[name=client]').val(unit);
						$('input[name=proxystylesheet]').val(unit);
					}
					
			});
		},
		
		TranslateSetup: function(){  // adds necessary meta and script elements to enable the google translate widget
		
			$('head').append('<meta name="google-translate-customization" content="2a9dc37f185c2a04-5b8a07c26cc72150-g88055c3034a40aa0-8" /><script type="text/javascript">function googleTranslateElementInit() {new google.translate.TranslateElement({pageLanguage: "en", layout: google.translate.TranslateElement.FloatPosition.BOTTOM_RIGHT, gaTrack: true, gaId: "UA-34361385-2"}, "google_translate_element");}</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>');				

		}
	};