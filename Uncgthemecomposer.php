<?php
use Composer\Script\Event;

/**
 * Installer to be used when composer is installing the base app.
 */
class Uncgthemecomposer
{
    public static function setupSymlinks(Event $event)
    {
        $paths = self::_helperDirectory($event);

        $io = $event->getIO();

        $f = '/public/themes/uncg';
        @unlink($paths['basePath'] . $f);
        symlink($paths['uncgthemeVendorPath'], $paths['basePath'] . $f);
        $io->write('Symlinking /public/themes/uncg');

        $f = '/incuncg';
        @unlink($paths['basePath'] . $f);
        symlink($paths['hostUncgAssetFolderPath'], $paths['basePath'] . $f);
        $io->write('Symlinking ' . $f);

    }

    public static function _helperDirectory(Event $event)
    {
        return array(
            'basePath'                  => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/../'),
            'uncgthemeVendorPath'       => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/uncgits/uncg-otf-theme/uncg'),
            'uncgAssetFolderPath'       => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/uncgits/uncg-otf-theme/uncg/incuncg'),
            'hostUncgAssetFolderPath'    => realpath( '/var/www/uncg-brand-assets/incuncg'),
        );
    }

}